//Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos//

numero = 33;

if (numero >= 10 && numero <= 99) {
  const digito1 = parseInt(numero / 10);
  const digito2 = parseInt(numero % 10);

  const suma = digito1 + digito2;

  const mensaje = 'La suma de ' + digito1 + ' + ' + digito2 + ' = ' + suma;

  console.log(mensaje);
} else {
  console.log('El numero no cumple con los requisitos!');
}