//Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo//

numero = 1445

if(numero >= 1000 && numero <= 9999){
    digito2 = numero % 10
    digito3 = numero % 10
    if (digito2 == digito3){
        console.log("Digitos son iguales")
    }
    else{
        console.log("Digitos no son iguales")
    }
}
else{
    console.log("Numero invalido")
}