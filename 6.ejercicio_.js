//Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro//

numero1 = 12
numero2 = 2

if(numero1 > 0 && numero2 > 0){
    digito1 = numero1 % 10
    digito2 = numero2 % 10
    if (digito1 % digito2 == 0){
        console.log("Digito si es multiplo")
    }
    else{
        console.log("Digito no es multiplo")
    }
}
else{
    console.log("Numero invalido")
}
